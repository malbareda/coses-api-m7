
//js es un llenguatge interpretat. va a executar el primer que trobi


//fetch, recupera dades d'una URL.
//El fetch es fa servir principalment per a recuperar dades JSON d'API
fetch('https://httpbin.org/json')  /// <--- cambiar URL
    //Cuando haya ejecutado esto, ejecuta el then. Es Asíncrono
    .then(function(response) {
        //resposta que torna. Com que la resposta es en json, tornem un json
        //return response.text();
        return response.json();   /// <---- seleccionar uno de los (generalmente JSON)
    })
	//recuperes la resposta com a data
    .then(function(data) {
        //la funcion que quieras hacer. 
		//com que json es un objecte de javascript pots accedir a l'element que vulguis facilment
        console.log('resultat = ', data.slideshow.slides[0].title);
        console.log('json complet ', data);

        //modificacion del DOM
        document.getElementById('misCosas').innerHTML = data.slideshow.slides[0].title;
    })
	//si hi ha un error ho poses per pantalla
    .catch(function(err) {
        console.error(err);
    });

 