package CountDownLatch;


import java.util.concurrent.CountDownLatch; 
  
public class Main 
{ 
    public static void main(String args[])  
                   throws InterruptedException 
    { 
        //creem un comptador de latch a 4
        CountDownLatch latch = new CountDownLatch(4); 
  
        //creem 4 threads 
        Alumne first = new Alumne(1000, latch,  
                                  "ALUMNO-1"); 
        Alumne second = new Alumne(2000, latch,  
                                  "ALUMNO-2"); 
        Alumne third = new Alumne(3000, latch,  
                                  "ALUMNO-3"); 
        Alumne fourth = new Alumne(4000, latch,  
                                  "ALUMNO-4"); 
        first.start(); 
        second.start(); 
        third.start(); 
        fourth.start();
        
        
        
    } 
} 