package CountDownLatch;

import java.util.concurrent.CountDownLatch;


//alumne que hace ver que vota
class Alumne extends Thread {
	private int delay;
	
	//CountDownLatch es un contador. Lo instancias con un numero
		//countDown, decrementa el contador.
		//await, esperar. Espera hasta que el contador este en 0. Luego ejecuta para todos los que esten esperando.

	
	private CountDownLatch latch;

	public Alumne(int delay, CountDownLatch latch, String name) {
		super(name);
		this.delay = delay;
		this.latch = latch;
	}

	@Override
	public void run() {
		try {
			//dormimos el delay. Cada Thread tiene un delay distinto.
			Thread.sleep(delay);
			System.out.println("YO "+this.getName()+" VOTO A TAL");
			//DECREMENTAS EL CONTADOR
			latch.countDown();
			System.out.println("YO "+this.getName()+" HE VOTADO");
			
			//ESPERAS
			latch.await();
			
			System.out.println("YO "+this.getName()+" VEO QUE YA HAN VOTADO TODOS");
			
			///aqui haces lo que quieras
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}